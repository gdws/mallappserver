<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
    <script src="<?php echo STATICS;?>/jquery-1.4.2.min.js" type="text/javascript"></script>
    <meta http-equiv="x-ua-compatible" content="ie=7" />
</head>
<body class="warp">
<div id="artlist" class="addn">
    <form class="form" method="post" action="" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="addn">
        <input type="hidden" name="pid" value="<?php echo ($cat_pid); ?>" />
        <tr>
            <th colspan="4">商品分类编辑</th>
        </tr>
        <?php if($cat_pid > 0): ?><tr>
                <td height="48" align="right"><strong>父分类名称：</strong></td>
                <td><b><?php echo ($parentname); ?></b></td>
            </tr><?php endif; ?>
        <tr>
            <td height="48" align="right"><strong>分类名称(中文)：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" id="title" name="title" value="<?php echo ($set["title"]); ?>" size="20"  class="px require"  />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>分类名称(英文)：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" id="name" name="name" value="<?php echo ($set["name"]); ?>" size="20"  class="px require"  />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>简介：</strong></td>
            <td><textarea name="des" class="px" style="width:400px;height:80px;"><?php echo ($set["des"]); ?></textarea></td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>排序：</strong></td>
            <td><input type="text" name="sort" value="<?php echo ($set["sort"]); ?>" id="sort" class="px" style="width:40px;"/>数字越小排再越前（大于等于0的整数）</td>
        </tr>
        <tr>
            <td colspan="4">
                    <input class="bginput" type="submit"  value="修 改">
                &nbsp;
                <input class="bginput" type="button" onclick="javascript:history.back(-1);" value="返 回" ></td>
        </tr>
    </table>
    </form>
    <br />
    <br />
    <br />
</div>
</body>
</html>