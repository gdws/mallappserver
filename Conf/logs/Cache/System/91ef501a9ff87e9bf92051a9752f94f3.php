<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>权限管理</title>
    <link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
<script src="<?php echo STATICS;?>/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo STATICS;?>/function.js" type="text/javascript"></script>
<meta http-equiv="x-ua-compatible" content="ie=7" />
</head>
<body class="warp">
<div id="artlist">
	<div class="mod kjnav">
		<?php if(is_array($nav)): $i = 0; $__LIST__ = $nav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a href="<?php echo U($action.'/'.$vo['name'],array('pid'=>$_GET['pid'],'level'=>3,'title'=>urlencode ($vo['title'])));?>"><?php echo ($vo['title']); ?></a>
			 <?php if(($action == 'Article') or ($action == 'Img') or ($action == 'Text') or ($action == 'Voiceresponse')): break; endif; endforeach; endif; else: echo "" ;endif; ?>
	</div>
</div>

    <form name="form1" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="alist">
            <tr>
                <!--店铺名，图片，店铺描述，类别id，类别名，店铺编号，楼层，-->
                <td width="10">选中</td>
                <td width="10">ID</td>
                <td width="10">关键字</td>
                <td width="20">搜索次数</td>
                <td width="15">是否热门</td>
                <td width="40" align='center'>关联店铺</td>
                <td width="100">管理操作</td>
            </tr>
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                    <td align='center'><input type="checkbox" name='test[]' value="<?php echo ($vo["id"]); ?>"/></td>
                    <td><?php echo ($vo["id"]); ?></td>
                    <td ><b><?php echo ($vo["keyword"]); ?></b></td>
                    <td ><?php echo ($vo["search_num"]); ?></td>
                    <td ><?php if(($vo["hot"]) == "1"): ?><font color="red">√</font><?php else: ?><font color="blue">×</font><?php endif; ?></td>
                    <td align='center'><?php echo ($vo["store_name"]); ?></td>
                    <td align='center'>
                        <a href="<?php echo U('Keywords/edit/',array('id'=>$vo['id']));?>">修改</a>    |
                        <?php if(($vo["username"]) == "admin"): ?><font color="#cccccc">删除</font>
                        <?php else: ?>
                            <a href="javascript:void(0)"
                               onclick="if(confirm('确定删除吗')){
                               location.href='<?php echo U('Keywords/del/',array('id'=>$vo['id']));?>'}">删除</a><?php endif; ?>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            <tr bgcolor="#FFFFFF">
                <td colspan="9"><div class="listpage"><?php echo ($page); ?></div></td>
            </tr>

        </table>
    </form>
    <script type="text/javascript">
        $(function(){
            $('#checkAll').click(function(){
                if($(this).attr('checked')){
                    $(':checkbox').attr('checked','true');
                }else{
                    $(':checkbox').removeAttr('checked');
                }
            });
        });
    </script>
    </body>
</html>