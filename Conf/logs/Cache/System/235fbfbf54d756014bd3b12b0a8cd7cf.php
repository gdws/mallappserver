<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
    <script src="<?php echo STATICS;?>/jquery-1.4.2.min.js" type="text/javascript"></script>
    <meta http-equiv="x-ua-compatible" content="ie=7" />
    <!--上传插件-->
    <script src="/tpl/static/artDialog/jquery.artDialog.js?skin=default"></script>
    <script src="/tpl/static/artDialog/plugins/iframeTools.js"></script>
    <script>
        function setlatlng(longitude,latitude){
            art.dialog.data('longitude', longitude);
            art.dialog.data('latitude', latitude);
            // 此时 iframeA.html 页面可以使用 art.dialog.data('test') 获取到数据，如：
            // document.getElementById('aInput').value = art.dialog.data('test');
            art.dialog.open('<?php echo U('Map/setLatLng_amap',array('token'=>$token,'id'=>$id));?>',{lock:false,title:'设置经纬度',width:600,height:400,yesText:'关闭',background: '#000',opacity: 0.87});
        }
    </script>
</head>
<body class="warp">
<div id="artlist" class="addn">
    <form action="" method="post" name="form" id="myform">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="addn">
        <tr>
            <th colspan="4"><?php echo ($title); ?></th>
        </tr>

        <tr>
            <td height="48" align="right"><strong>爆品名：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="name" class="ipt" size="45" value="<?php echo ($set["name"]); ?>"  class="px require"  />
            </td>
        </tr>

        <tr>
            <td height="48" align="right"><strong>图标 ：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="logourl" id="logourl"  class="ipt" size="45" value="<?php echo ($set["logourl"]); ?>" class="px require"  />
                <script src="/tpl/static/upyun.js"></script>
                <a href="###" onclick="upyunPicUpload('logourl',700,420,'')" class="a_upload">上传</a> <a href="###" onclick="viewImg('logourl')">预览</a>
            </td>
        </tr>

        <tr>
            <td height="48" align="right"><strong>爆品价：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" name="price" class="ipt" size="45" value="<?php echo ($set["price"]); ?>"   class="px require"  />
            </td>
        </tr>

        <tr>
            <td height="48" align="right"><strong>原价：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="oprice" class="ipt" size="45"  value="<?php echo ($set["oprice"]); ?>"     class="px require"  />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>开始时间：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="starttime" class="ipt" size="45" value="<?php echo (date("Y-m-d",$set["starttime"])); ?>"   class="px require"  />
            <span>&nbsp;&nbsp;格式：2015-06-07</span>
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>结束时间：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="endtime" class="ipt" size="45"  value="<?php echo (date("Y-m-d",$set["endtime"])); ?>" class="px require"  />
                <span>&nbsp;&nbsp;格式：2015-06-07</span></td>
        </tr>

        <tr>
            <td  height="48" align="right"><strong>产品描述：</strong></td>
            <td><textarea	 type="text" name="des" class="ipt" style="width:450px;height:60px;margin:5px 0 5px 0;" /><?php echo ($set["des"]); ?></textarea><span>&nbsp;&nbsp;一般不超过200个字符</span>
        </tr>

        <tr>
            <td colspan="4">
                <input class="bginput" type="submit"  value="修 改">
                &nbsp;
                <input class="bginput" type="button" onclick="window.history.go(-1);window.location.reload();" value="返 回" ></td>
        </tr>
    </table>
    </form>
    <br />
    <br />
    <br />
</div>
</body>
</html>