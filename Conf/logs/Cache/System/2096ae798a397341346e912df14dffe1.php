<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
    <script src="<?php echo STATICS;?>/jquery-1.4.2.min.js" type="text/javascript"></script>
    <meta http-equiv="x-ua-compatible" content="ie=7" />
    <!--省市区插件-->
    <script type="text/javascript" src="<?php echo ($staticPath); ?>/tpl/static/address/PCASClass.js"> </script>
    <!--上传插件-->
    <script src="/tpl/static/artDialog/jquery.artDialog.js?skin=default"></script>
    <script src="/tpl/static/artDialog/plugins/iframeTools.js"></script>
    <script>
        function setlatlng(longitude,latitude){
            art.dialog.data('longitude', longitude);
            art.dialog.data('latitude', latitude);
            // 此时 iframeA.html 页面可以使用 art.dialog.data('test') 获取到数据，如：
            // document.getElementById('aInput').value = art.dialog.data('test');
            art.dialog.open('<?php echo U('Map/setLatLng_amap',array('token'=>$token,'id'=>$id));?>',{lock:false,title:'设置经纬度',width:600,height:400,yesText:'关闭',background: '#000',opacity: 0.87});
        }
    </script>
</head>
<body class="warp">
<div id="artlist" class="addn">
        <!--form表单-->
    <form action="<?php echo U('Mall/edit');?>" method="post" name="form" id="myform">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="addn">
        <tr>
            <th colspan="4">购物中心配置</th>
        </tr>
        <tr>
            <td height="48" align="right"><strong>购物中心名称：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" id="name" name="name" class="ipt" size="45" value="<?php echo ($set["name"]); ?>" class="px require"  />
            </td>
        </tr>
        <tr>
        <tr>
            <td height="48" align="right"><strong>门店地址：</strong></td>
            <td colspan="3" class="lt">
                <select name="province"></select>
                <select name="city"></select>
                <select name="district"></select>
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>详细地址：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" id="address" name="address"  class="ipt" size="45" value="<?php echo ($set["address"]); ?>" class="px require" />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>Logo地址：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" id="logourl" name="logourl"  class="ipt" size="45" value="<?php echo ($set["logourl"]); ?>" class="px require" class="ipt" size="45" />
                <script src="/tpl/static/upyun.js"></script>
                <a href="###" onclick="upyunPicUpload('logourl',700,420,'')" class="a_upload">上传</a> <a href="###" onclick="viewImg('logourl')">预览</a>
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>经纬度：</strong></td>
            <td>
                经度 <input type="text" id="longitude"  name="longitude" size="14" class="px" value="<?php echo ($set["longitude"]); ?>" />
                纬度 <input type="text"  name="latitude" size="14" id="latitude" class="px" value="<?php echo ($set["latitude"]); ?>" />
                <a href="###" onclick="setlatlng($('#longitude').val(),$('#latitude').val())">在地图中查看/设置</a>
            </td>
        </tr>

        <tr>
            <td colspan="4">
                    <input class="bginput" type="submit" name="dosubmit" value="保存设置" >
                &nbsp;
            </td>
        </tr>
    </table>
    </form>
    <br />
    <br />
    <br />
</div>

<script language="javascript">
    <!--省市区插件-->
    $(function(){
        new PCAS("province","city","district","<?php echo ($set["province"]); ?>","<?php echo ($set["city"]); ?>","<?php echo ($set["district"]); ?>");
        $("#form").valid([
            { name:"name",simple:"名称",require:true}

        ],true,true);

    })
</script>
</body>
</html>