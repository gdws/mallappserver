<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>分类管理</title>
<!--自定义导航菜单 不用默认common:public-->
<link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
<script src="<?php echo STATICS;?>/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo STATICS;?>/function.js" type="text/javascript"></script>
<meta http-equiv="x-ua-compatible" content="ie=7" />
</head>
<body class="warp">
<div id="artlist">
    <div class="mod kjnav">
        <a href="<?php echo U('System/Category/add',array('cat_pid'=>$cat_pid));?>">添加分类</a>
        <?php if($cat_pid > 0): ?><a href="<?php echo U('System/Category/index',array('cat_pid'=>0));?>">返回上级</a><?php endif; ?>
    </div>
</div>


<div class="cr"> </div>
    <form name="form1" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="alist">
            <tr>
                <td width="20">ID</td>
                <td width="150">分类名称</td>
                <td width="150">简介</td>
                <td width="100">创建时间</td>
                <td width="100">操作</td>
            </tr>
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$hostlist): $mod = ($i % 2 );++$i;?><tr>
                    <td><input type="checkbox" name="del_id[]" value="" class="checkitem"></td>
                    <td><?php echo ($hostlist["title"]); ?></td>
                    <td><?php echo ($hostlist["des"]); ?></td>
                    <td><?php echo (date("Y-m-d H:i:s",$hostlist["time"])); ?></td>
                    <td class="norightborder">
                        <a href="<?php echo U('Category/edit',array('id' => $hostlist['id']));?>">修改</a>
                        <a href="javascript:drop_confirm('您确定要删除吗?', '<?php echo U('Category/del',array('id'=>$hostlist['id']));?>');">删除</a>
                        <?php if($hostlist['pid'] == 0): ?><span>&nbsp;|&nbsp;
                                <a href="<?php echo U('Category/index',array('cat_pid' =>$hostlist['id']));?>" style="color:#f00">子分类列表</a></span><?php endif; ?>
                    </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            <tr bgcolor="#FFFFFF">
                <td colspan="9"><div class="listpage"><?php echo ($page); ?></div></td>
            </tr>
        </table>
    </form>
<script type="text/javascript">
    function drop_confirm(msg, url){
        if(confirm(msg)){
            window.location = url;
        }
    }
</script>
    </body>
</html>