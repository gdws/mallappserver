<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
    <!--<script src="<?php echo STATICS;?>/jquery-1.4.2.min.js" type="text/javascript"></script>-->
    <meta http-equiv="x-ua-compatible" content="ie=7" />

    <!--下拉框多选-->
    <!--<script type="text/javascript" src="/tpl/static/selectmultile/js/jquery-1.10.2.min.js"></script>-->
 

    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>

    <script type="text/javascript" src="/tpl/static/selectmultile/js/bootstrap-select.js"></script>
    <link rel="stylesheet" type="text/css" href="/tpl/static/selectmultile/css/bootstrap-select.css">
    <link rel="stylesheet" href="/tpl/static/selectmultile/css/bootstrap.min.css" >
    <script src="/tpl/static/selectmultile/js/bootstrap.min.js"></script>
</head>
<body class="warp">
<div id="artlist" class="addn">
    <form action="" method="post" name="form" id="myform">
    <table width="100%"  border="0" cellspacing="0" cellpadding="0" id="addn">
        <tr>
            <th colspan="4"><?php echo ($title); ?></th>
        </tr>

        <tr>
            <td height="48" align="right"><strong>店铺名：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="name" class="ipt" size="45"  class="px require"  />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>店铺英文名：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="alias" class="ipt" size="45"  class="px require"  />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>图标 ：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="logourl" id="logourl"  class="ipt" size="45"  class="px require"  />
                <script src="/tpl/static/upyun.js"></script>
                <a href="###" onclick="upyunPicUpload('logourl',700,420,'')" class="a_upload">上传</a> <a href="###" onclick="viewImg('logourl')">预览</a>
            </td>
        </tr>

        <tr>
            <td height="48" align="right"><strong>人均价格：</strong></td>
            <td colspan="3" class="lt">
                <input type="text" name="average_price"  class="ipt"  onfocus="if(this.value=='请输入数字')this.value=''" onblur="if(this.value=='')this.value='请输入数字'" value="请输入数字"  maxlength="4" onkeyup='this.value=this.value.replace(/\D/gi,"")'/>
            </td>
        </tr>

        <!-- 下拉多选 -->
        <tr>
            <td height="48" align="right"><strong title="让搜索更易找到">选择关键字：</strong></td>
            <td>
                <select id="id_select"  class="selectpicker  form-control" multiple data-live-search="true">
                    <?php if(is_array($multi_select)): $i = 0; $__LIST__ = $multi_select;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" <?php echo ($vo["selected"]); ?>><?php echo ($vo["keyword"]); ?>   </option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>&nbsp 让搜索更易找到
            </td>
        </tr>

        <!-- 多标签 -->
        <tr id="jq124">
            <td width="219" height="52" align="right" valign="middle" >
                <label class="i_zt"   ><span style="color:#da4453;">*</span> 主办单位：</label>
            </td>
            <td height="52" valign="middle">
                <!-- 这里调整框大小 -->
                <div class="i_inp"  id="paren"  style="  min-height:30px; cursor:pointer; height:auto;width: 600px;">
                <span id="tags" class="ui-sortable disp"  >
<!--循环-->
     <span id="dc" class="sort" style=" background:#CCC;   white-space:nowrap; float:left; display:block;   margin:5px;   height:22px; font-size:12px; "><div id="dart" style="padding:0px 6px; cursor:move; height:22px; display:block; float:left;   line-height:22px;">啊</div><img onclick=" $(this).parent().remove();" src="/tpl/static/input_multi_tags/images/sfwrg.jpg" style="float:left; display:block; cursor:pointer; "></span>
     <span id="dc" class="sort" style=" background:#CCC;   white-space:nowrap; float:left; display:block;   margin:5px;   height:22px; font-size:12px; "><div id="dart" style="padding:0px 6px; cursor:move; height:22px; display:block; float:left;   line-height:22px;">额</div><img onclick=" $(this).parent().remove();" src="/tpl/static/input_multi_tags/images/sfwrg.jpg" style="float:left; display:block; cursor:pointer; "></span>
                    <!--循环end -->
                </span>
                    <input class="ipva" name=""    type="text" style=" border:none; float:left; display:block; width:auto; outline:none; margin:5px; width:auto; min-width:10px; max-width:200px; background:none; height:22px; line-height:22px; font-size:12px; font-family:微软雅黑;" />
                    <div style="clear:both;"></div>
                </div>
            </td>
            <td width="500">
                <table   border="0" cellspacing="0" cellpadding="0" class="did">
                    <tr>
                        <td height="3"><img src="/tpl/static/input_multi_tags/images/eeee_03.png" alt="" /></td>
                        <td height="3" style="background:url(/tpl/static/input_multi_tags/images/eeee_05.png) repeat-x #f9f9f9"></td>
                        <td height="3"><img src="/tpl/static/input_multi_tags/images/eeee_06.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td width="30" valign="top"  style="background:url(/tpl/static/input_multi_tags/images/err_11.png) repeat-y #f9f9f9"><img src="/tpl/static/input_multi_tags/images/eeee_08.png" alt="" /></td>
                        <td style="background:#f9f9f9; line-height:16px; padding:5px 3px; font-size:12px; font-family:宋体;">输入后<span style="color:#da4453;">回车</span>即可添加更多关键字
                        </td>
                        <td style="background:url(/tpl/static/input_multi_tags/images/eeee_10.jpg) repeat-y #f9f9f9"></td>
                    </tr>
                    <tr>
                        <td><img src="/tpl/static/input_multi_tags/images/eeee_11.png" alt="" /></td>
                        <td style="background:url(/tpl/static/input_multi_tags/images/eeee_12.png) repeat-x #f9f9f9"></td>
                        <td><img src="/tpl/static/input_multi_tags/images/eeee_13.png" alt="" /></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="4">
                <input class="bginput" id="buttensubmit" type="button"  value="添 加">
                &nbsp;
                <input class="bginput" type="button" onclick="javascript:history.back(-1);" value="返 回" ></td>
        </tr>
    </table>
    </form>
    <br />
    <br />
    <br />
</div>
<script>
    $(function(){

        /*  下拉框多选 start */
        $('.selectpicker').selectpicker({
            'selectedText': 'cat'
        });
        /*  下拉框多选 end */

        $(".sanjiao").click(function(){
            $(this).parent().find("ul").toggle();
        })

        $("#theButton").click( function (){
//        var a1=$('#myTags a');    //方法和上面一样
            var tags = $("#tags").children();//全部子节点；
//          alert(tags.length);return
            var buf = [];
            for(var i=0;i<tags.length;i++) {
                var list =  $(tags[i]).children();
                buf.push($(list[0]).text());
            }
            alert(buf.join(','));
        })

        $(".iise li ul").hide()
        $(".i_iop").focus(function(){		     $(this).parent().parent().find("td:eq(2) .did").fadeIn();  })
        $(".i_iop").blur(function(){		     $(this).parent().parent().find("td:eq(2) .did").fadeOut(1);  })
        $(".i_inp .ipva").focus(function(){
            $(this).parent().parent().parent().find("td:eq(2) .did").fadeIn();
        })
        $(".i_inp .ipva").blur(function(){
            $(this).parent().parent().parent().find("td:eq(2) .did").fadeOut(1);
        })
    })

    function submitbt(){
        if($(".i_iop:eq(0)").val() == ""){
            $(".i_iop:eq(0)").focus();
            $(".i_iop:eq(0)").parent().parent().find("td:eq(2) table.did tr:eq(1) td:eq(1)").html("<span style='color:#f00;' >此项内容为必填，不能为空。</span>")
        }
        $(".i_iop:eq(0)").blur(function(){    $(this).parent().parent().find("td:eq(2) table.did tr:eq(1) td:eq(1)").html("主标题")    })
    }
</script>

<!--input框多标签-->
<link href="/tpl/static/input_multi_tags/css/all.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="/tpl/static/input_multi_tags/js/biankuai.js"></script>
<script language="javascript" src="/tpl/static/input_multi_tags/js/ui.base.min.js"></script>
<script language="javascript" src="/tpl/static/input_multi_tags/js/ui.droppable.min.js"></script>
<script language="javascript" src="/tpl/static/input_multi_tags/js/ui.draggable.min.js"></script>
<script language="javascript" src="/tpl/static/input_multi_tags/js/ui.sortable.min.js"></script>
<script language="javascript" src="/tpl/static/input_multi_tags/js/drag.js"></script> <!--mise有问题-->
<script language="javascript" src="/tpl/static/input_multi_tags/js/drag1.js"></script>
<script language="javascript" src="/tpl/static/input_multi_tags/js/submit.js"></script>


</body>
</html>