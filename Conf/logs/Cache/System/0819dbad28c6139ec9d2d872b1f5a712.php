<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo RES;?>/images/main.css" type="text/css" rel="stylesheet">
    <script src="<?php echo STATICS;?>/jquery-1.4.2.min.js" type="text/javascript"></script>
    <meta http-equiv="x-ua-compatible" content="ie=7" />

    <!--节点树-->
    <!-- 节点树样式 -->
    <link href="/tpl/static/checkboxtree/css/tree.css" rel="stylesheet" type="text/css" />
    <!-- 节点树js -->
    <script src="/tpl/static/checkboxtree/js/jquery.js" type="text/javascript"></script>
    <script src="/tpl/static/checkboxtree/js/Plugins/jquery.tree.js" type="text/javascript"></script>

    <!--下拉框多选 -->
    <script type="text/javascript" src="/tpl/static/selectmultile/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/tpl/static/selectmultile/js/bootstrap-select.js"></script>
    <link rel="stylesheet" type="text/css" href="/tpl/static/selectmultile/css/bootstrap-select.css">
    <link href="/tpl/static/selectmultile/css/bootstrap.min.css" rel="stylesheet">
    <script src="/tpl/static/selectmultile/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(window).on('load', function () {
            $('.selectpicker').selectpicker({
                'selectedText': 'cat'
            });
//             $('.selectpicker').selectpicker('hide');

            $("#buttensubmit").click(function(e){
                $('#selectvalue').val($('#id_select').val());
                $('#myform').submit();
            });
        });
    </script>
</head>
<body class="warp">
<div id="artlist" class="addn">
    <form action="" method="post" name="form" id="myform">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="addn">
        <input type="hidden" name="id" value="<?php echo ($id); ?>" />
        <tr>
            <th colspan="4"><?php echo ($title); ?></th>
        </tr>
        <tr>
            <td height="48" align="right"><strong>关键字：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="name" value="<?php echo ($edit["keyword"]); ?>" readonly class="ipt" size="45"  class="px require"  />
            </td>
        </tr>
        <tr>
            <td height="48" align="right"><strong>搜索次数：</strong></td>
            <td colspan="3" class="lt">
                <input type="text"  name="alias" value="<?php echo ($edit["search_num"]); ?>" class="ipt" size="45"  class="px require"  />
            </td>
        </tr>
        <tr>
            <td  height="48" align="right"><strong>是否热门：</strong></td>
            <td><input type="radio" name="hot" value="1" <?php if($edit['hot']==1) echo checked; ?> /> 是 <input type="radio" name="hot" value="0" <?php if($edit['hot']==0)echo checked; ?> /> 否</td>
        </tr>

        <tr>
            <td height="48" align="right"><strong>关联店铺：</strong></td>
            <td>
                <select id="id_select"  class="selectpicker  form-control" multiple data-live-search="true">
                    <?php if(is_array($select_categorys)): $i = 0; $__LIST__ = $select_categorys;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>" <?php echo ($vo["selected"]); ?>><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </td>
        </tr>


<input type="hidden" id="selectvalue"  name="selectvalue" value="" class="ipt" size="25"  class="px require"  />
        <tr>
            <td colspan="4">
                <!--<input class="bginput" type="submit"  value="修  改">-->
                <input class="bginput" id="buttensubmit" type="button"  value="修  改">
                &nbsp;
                <input class="bginput" type="button" onclick="javascript:history.back(-1);" value="返 回" ></td>
        </tr>
    </table>
    </form>
    <br />
    <br />
    <br />
</div>
</body>
</html>