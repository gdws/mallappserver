<?php
/**
 * 数据库配置文件
 *
 */
if (!defined('THINK_PATH'))	exit('非法调用');
/*本地数据库*/
/**/
return array(
		'DB_TYPE'=>'mysql',
		'DB_HOST'=>'localhost',
		'DB_NAME'=>'mallapp_test',
		'DB_USER'=>'root',
		'DB_PWD'=>'root',
		'DB_PORT'=>'3306',
		'DB_PREFIX'=>'pigcms_',
		'agent_version'=>1
);