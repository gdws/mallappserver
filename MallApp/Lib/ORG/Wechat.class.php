<?php
class Wechat
{
	public $token;
	public $wxuser;
	private $data = array();
	/**
	 * Wechat constructor.
	 * @param $token
	 * @param string $wxuser
     */
	public function __construct($token, $wxuser = '')
	{
		$this->auth($token, $wxuser) || exit();
		if (IS_GET) {
			echo $_GET['echostr'];
			exit();
		} else {
			$this->token = $token;
			if (!$wxuser) {
				$wxuser = M('wxuser')->where(array('token' => $this->token))->find();
			}
			$this->wxuser = $wxuser;
			if ($this->wxuser['type'] == 1) {
				$account_info = M('Weixin_account')->where('type=1')->field('token,appId,encodingAesKey')->find();
				$this->wxuser['pigsecret'] = $account_info['token'];
				$this->wxuser['appid'] = $account_info['appId'];
				$this->wxuser['aeskey'] = $account_info['encodingAesKey'];
			}
			else if (empty($this->wxuser['pigsecret'])) {
				$this->wxuser['pigsecret'] = $this->token;
			}
			/* 获取xml消息 */
			$xml = file_get_contents('php://input');
			if ($this->wxuser['encode'] == 2) {
				$this->data = $this->decodeMsg($xml);
			} else {
				$xml = new SimpleXMLElement($xml);   // 将xml转成对象
				$xml || exit();
				foreach ($xml as $key => $value) {
					$this->data[$key] = strval($value);
				}
			}
		}
	}

	public function encodeMsg($sRespData)
	{
		$sReqTimeStamp = time();
		$sReqNonce = $_GET['nonce'];
		$encryptMsg = '';
		import('@.ORG.aes.WXBizMsgCrypt');
		$pc = new WXBizMsgCrypt($this->wxuser['pigsecret'], $this->wxuser['aeskey'], $this->wxuser['appid']);
		$sRespData = str_replace('<?xml version="1.0"?>', '', $sRespData);
		$errCode = $pc->encryptMsg($sRespData, $sReqTimeStamp, $sReqNonce, $encryptMsg);
		if ($errCode == 0) {
			return $encryptMsg;
		}
		else {
			return $errCode;
		}
	}

	public function decodeMsg($msg)
	{
		import('@.ORG.aes.WXBizMsgCrypt');
		$sReqMsgSig = $_GET['msg_signature'];
		$sReqTimeStamp = $_GET['timestamp'];
		$sReqNonce = $_GET['nonce'];
		$sReqData = $msg;
		$sMsg = '';
		$pc = new WXBizMsgCrypt($this->wxuser['pigsecret'], $this->wxuser['aeskey'], $this->wxuser['appid']);
		$errCode = $pc->decryptMsg($sReqMsgSig, $sReqTimeStamp, $sReqNonce, $sReqData, $sMsg);

		if ($errCode == 0) {
			$data = array();
			$xml = new SimpleXMLElement($sMsg);
			$xml || exit();

			foreach ($xml as $key => $value) {
				$data[$key] = strval($value);
			}

			return $data;
		}
		else {
			return $errCode;
		}
	}

	public function request()
	{
		return $this->data;
	}

	public function response($content, $type = 'text', $flag = 0)
	{
		$this->data = array('ToUserName' => $this->data['FromUserName'], 'FromUserName' => $this->data['ToUserName'], 'CreateTime' => NOW_TIME, 'MsgType' => $type);
		$this->$type($content);  // news,text
		$this->data['FuncFlag'] = $flag;
		$xml = new SimpleXMLElement('<xml></xml>');

		$this->data2xml($xml, $this->data);   // 数组转xml
		if (isset($_GET['encrypt_type']) && ($_GET['encrypt_type'] == 'aes')) {
			exit($this->encodeMsg($xml->asXML()));
		}
		else {
			file_put_contents('./test/respons.xml',$xml->asXML());
			exit($xml->asXML());
		}
	}

	private function text($content)
	{
		$this->data['Content'] = $content;
	}

	private function music($music)
	{
		list($music['Title']) = $music;
		$this->data['Music'] = $music;
	}

	private function news($news)
	{  /*input
  		array(1) {
				  [0] => array(4) {
						[0] => string(33) "省钱 打折 促销 优先知道"
						[1] => string(75) "尊贵vip，是您消费身份的体现，省钱 打折 促销 优先知道"
						[2] => string(59) "http://weixin.goldenwingsinfo.com/tpl/static/images/vip.jpg"
						[3] => string(133) "http://weixin.goldenwingsinfo.com/index.php?g=Wap&m=Card&a=card&token=iroxkr1450231038&cardid=1&wecha_id=opGyTv3J1MGdBjfStZy6jWoichIA"
					 }
  		 } */
		$articles = array();
		foreach ($news as $key => $value) {  // list:相当于栈，将数组元素反向添加
			list($articles[$key]['Title'], $articles[$key]['Description'], $articles[$key]['PicUrl'], $articles[$key]['Url']) = $value;
			if (9 <= $key) {	// 最多10条图文
				break;
			}
		}
		/*output
		 * array(1) {
			  [0] => array(4) {
				["Url"] => string(133) "http://weixin.goldenwingsinfo.com/index.php?g=Wap&m=Card&a=card&token=iroxkr1450231038&cardid=1&wecha_id=opGyTv3J1MGdBjfStZy6jWoichIA"
				["PicUrl"] => string(59) "http://weixin.goldenwingsinfo.com/tpl/static/images/vip.jpg"
				["Description"] => string(75) "尊贵vip，是您消费身份的体现，省钱 打折 促销 优先知道"
				["Title"] => string(33) "省钱 打折 促销 优先知道"
			  }
			}
		 */
		$this->data['ArticleCount'] = count($articles);
		$this->data['Articles'] = $articles;
	}

	private function transfer_customer_service($content)
	{
		$this->data['Content'] = '';
	}

	private function data2xml($xml, $data, $item = 'item')
	{
		foreach ($data as $key => $value) {
			is_numeric($key) && ($key = $item);
			if (is_array($value) || is_object($value)) {
				$child = $xml->addChild($key);
				$this->data2xml($child, $value, $item);
			}
			else if (is_numeric($value)) {
				$child = $xml->addChild($key, $value);
			}
			else {
				$child = $xml->addChild($key);
				$node = dom_import_simplexml($child);
				$node->appendChild($node->ownerDocument->createCDATASection($value));
			}
		}
	}

	private function auth($token, $wxuser = '')
	{
		$signature = $_GET['signature'];
		$timestamp = $_GET['timestamp'];
		$nonce = $_GET['nonce'];

		if (!$wxuser) {
		}

		if ($wxuser && strlen($wxuser['pigsecret'])) {
		}

		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode($tmpArr);
		$tmpStr = sha1($tmpStr);

		if (trim($tmpStr) == trim($signature)) {
			return true;
		}
		else {
			return true;
		}

		return true;
	}
} 
?>
