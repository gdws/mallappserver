<?php
    class Product_catModel extends Model{
        protected $_validate = array(
            array('title','require','名称不能为空！'),
//            array('title','','名称已经存在！',0,'unique',1),
//          array('title','','帐号名称已经存在！',2,'unique',1), // 在新增的时候验证name字段是否唯一
//             array('name','checkNode','节点已经存在',0,'callback'),
         );
//    protected $_auto = array (
//        array('token','gettoken',1,'callback'),
//        array('time','time',1,'function')
//    );
//    function gettoken(){
//        return session('token');
//    }
//        public function checkNode() {
//            $map['name']	 =	 $_POST['name'];
//            $map['pid']	=	isset($_POST['pid'])?$_POST['pid']:0;
//            $map['status'] = 1;
//            if(!empty($_POST['id'])) {
//                $map['id']	=	array('neq',$_POST['id']);
//            }
//            $result	=	$this->where($map)->field('id')->find();
//            if($result) {
//                return false;
//            }else{
//                return true;
//            }
//        }
        // 获取所有节点信息
        public function getAllNode($where = '' , $order = 'sort DESC') {
            return $this->where($where)->order($order)->select();
        }

        // 获取单个节点信息
        public function getNode($where = '',$field = '*') {
            return $this->field($field)->where($where)->find();
        }

        // 删除节点
        public function delNode($where) {
            if($where){
                return $this->where($where)->delete();
            }else{
                return false;
            }
        }

        // 更新节点
        public function upNode($data) {
            if($data){
                return $this->save($data);
            }else{
                return false;
            }
        }

        // 子节点
        public function childNode($id){
            return $this->where(array('pid'=>$id))->select();
        }
}

?>