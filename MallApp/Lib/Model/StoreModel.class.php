<?php
/*
 * SELECT b.title
 * 				FROM
 * pigcms_cat_store AS a, pigcms_product_cat AS b
 * 			 	WHERE
 * a.category_id = b.id AND a.store_id='3'
 */
class StoreModel extends RelationModel{
	protected $_link=array(
			"product_cat"=>array(
				"mapping_type"=>MANY_TO_MANY,
		        'class_name'    =>'product_cat',
				'mapping_name'=>'product_cat',
			    'mapping_fields'=>'pid,title',
//				'mapping_fields'=>array('title'),
//			    'condition'=>'title=\'女装\'',
				"foreign_key"=>"store_id",//中间表的字段
				"relation_foreign_key"=>"category_id",//中间表的字段
				"relation_table"=>"pigcms_cat_store",
//				"mapping_fields"=>"title",
			)
	);
}