<?php
/*
 * 用于关键字搜索店铺
 */
class StoreViewModel extends ViewModel {
	public $viewFields = array(
		'store'=>array(
				'id',
				'mall_id',
				'name',
				'logourl',
				'average_price',
				'mall_name',
				'area',
				'floor',
				'number',
				'is_groupon',
				'is_coupon',
				'is_new',
//				'_as'=>'store'
		),
		'keyword_store'=>array(
//				'keyword_id',
//				'store_id',
				'_on'=>'store.id=keyword_store.store_id'
		),
		'keywords'=>array(
				'keyword',
				'_on'=>'keyword_store.keyword_id=keywords.id'
		),
	);
}