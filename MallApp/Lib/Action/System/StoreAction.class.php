<?php
/*
 * 店铺管理
 */
class StoreAction extends BackAction
{
	public $store_model;
	public function _initialize() {
		$this->store_model = M('Store');
		// 判断是否存在购物中心
		if(!session('mall_id') || !session('mall_name')) {
			$where['admin_id'] = $_SESSION[C('USER_AUTH_KEY')];
			$mall = M('mall')->where($where)->find();
			if (!$mall) {
				$this->error('您还没有添加您的购物中心信息', U('Mall/add'));
			}
			session('mall_id',$mall['id']);
			session('mall_name',$mall['name']);
		}
		parent::_initialize();  //RBAC 验证接口初始化
	}
	// 店铺列表
	public function index() {
		$where = array('mall_id'=>session('mall_id'));
		if (IS_POST) { // 搜索
			$map['token'] = $this->get('token');
//			$map['name|des'] = array('like', '%' . $key . '%');
			$list = $this->store_model->where($map)->order('sort ASC, id DESC')->select();
			$count = $this->store_model->where($map)->count();
			$Page = new Page($count, 20);
			$show = $Page->show();
		} else {
			$count = $this->store_model->where($where)->count();
			$Page = new Page($count, 5);
			$show = $Page->show();
		    // 店铺名，图片，店铺描述，类别id，类别名，店铺编号，楼层
			$list = $this->store_model->where($where)->order('id DESC')->limit($Page->firstRow . ',' . $Page->listRows)->select();
		}
		$this->assign('page', $show);
		$this->assign('list', $list);   // 分类列表
		$this->display();
	}

	function test(){
		$Model = D("Store");
		$list = $Model->relation(true)->select();
//		echo $Model->getLastSql();die;
		echo "<pre>";
		print_r($list);
	}

	/*二维数组形成一个新的一维数组*/
	function getSubByKey($Array,$field){
		$arr = array();
		foreach($Array as $value){
			$arr[]=$value[$field];
		}
		return $arr;
	}


	/* 添加店铺 */
	public function add()
	{
		if (IS_POST) {
			$model_keywords = M('Keywords');  // 关键字数据表
			$_POST['mall_id']=session('mall_id');
			$_POST['mall_name']=session('mall_name');
			$_POST['add_time']=time();
			/* 店铺分类 start */
			$checked_categorys = array();
			$add_cate_keyword_ids = array();
			$select_cate_keyword_ids = array();
			$have_cate_keyword_ids = array();
			$add_cate_keywords = array();
			if (isset($_POST["checked_categorys"]) && !empty($_POST["checked_categorys"]) ) {
				$_POST['category_ids'] = $_POST["checked_categorys"]; // 分类id
				$checked_categorys = explode(',',$_POST["checked_categorys"]);
				$categorys_keyword_list= M('product_cat')->field('title')->where(array('id'=>array('IN',$checked_categorys)))->select();
		  	    $add_cate_keywords = $this->getSubByKey($categorys_keyword_list, 'title');

				$have = $model_keywords->field('id,keyword')->where(array('keyword'=>array('IN',$add_cate_keywords)))->select();
				if($have){
					$have_cate_keyword_ids = $this->getSubByKey($have, 'id');
					$have_cate_keywords = $this->getSubByKey($have, 'keyword');
					$add_cate_keywords = array_diff($add_cate_keywords,$have_cate_keywords);// 去掉已有关键字
				}
				foreach($add_cate_keywords as $keyword) { // 将新关键字存入数据库并获取id
					$add_cate_keyword_ids[] = $model_keywords->add(array('keyword'=>$keyword));
				}
				$select_cate_keyword_ids = array_merge($have_cate_keyword_ids,$add_cate_keyword_ids);
				unset($_POST["checked_categorys"]);
			}
			/* 店铺关键字 start*/
			$select_keyword_ids = array(); //
			if (isset($_POST["select_keywords"]) && !empty($_POST["select_keywords"]) ) {
				$select_keyword_ids = explode(',',$_POST["select_keywords"]);
				unset($_POST["select_keywords"]);
			}
			/* 添加关键字入数据库 */
			$have_keyword_ids = array(); // 存放已有关键字id
			$have_keywords = array();    // 存放已有关键字
			$add_keyword_ids = array();  // 存放添加关键字id
			if (isset($_POST["add_keywords"]) && !empty($_POST["add_keywords"]) ) {
				$add_keywords = explode(',',$_POST["add_keywords"]);
				$have = $model_keywords->field('id,keyword')->where(array('keyword'=>array('IN',$add_keywords)))->select();
			    if($have){
					$have_keyword_ids = $this->getSubByKey($have, 'id');
					$have_keywords = $this->getSubByKey($have, 'keyword');
					$add_keywords = array_diff($add_keywords,$have_keywords);
				}
				foreach($add_keywords as $keyword) {
					$add_keyword_ids[] = $model_keywords->add(array('keyword'=>$keyword));
				}
				$add_keyword_ids = array_merge($have_keyword_ids,$add_keyword_ids);
				unset($_POST["add_keywords"]);
			}
			// 整合所有keyword的id
			$select_keyword_ids = array_merge($select_cate_keyword_ids,$select_keyword_ids,$add_keyword_ids);
			/* 如果选择了关键字 */
			if($select_keyword_ids){
				$select_keyword_ids = array_unique($select_keyword_ids);
			}
			$store_id = $this->store_model->add($_POST);
			if($store_id){
				/*多对多 分类-店铺*/
				$Cat_store_model = M('Cat_store');
				$Cat_store_model->where(array('store_id'=>$store_id))->delete();
				foreach(checked_categorys as $v){
					$Cat_store_model->add(array('store_id'=>$store_id,'category_id'=>$v));
				}
				/*多对多 关键字-店铺*/
				$Keyword_store_model = M('Keyword_store');
				$Keyword_store_model->where(array('store_id'=>$store_id))->delete();// 先删除，再添加
				foreach($select_keyword_ids as $v) {
					$Keyword_store_model->add(array('store_id'=>$store_id,'keyword_id'=>$v));
				}
				$this->success('添加成功');
			} else {
				$this->error('添加失败');
			}
		} else {
			// 节点树
			$root = array("id"=>"0",
					"text"=>"选择分类",
					"value" =>"0",
					"showcheck"=>false,
					complete => true,
					"isexpand" => false,
					"checkstate"=>0,
					"hasChildren" => true);
			$Product_cat = D('Product_cat')->field('id,title')->where('pid=0')->select();
			$arr = array();
			foreach($Product_cat as $k=>$value){
				$subarr  = array();
				$Product_cat_son = D('Product_cat')->field('id,title')->where('pid='.$value['id'])->select();
				foreach($Product_cat_son as $kk => $vv){
					array_push($subarr,array(
							"id"=>"node-" . $k.'-'.$kk,
							"text"=>"{$vv['title']}",
							"value" =>"{$vv['id']}",
							"showcheck"=>true,
							complete => true,
							"isexpand" => true,
							"checkstate"=>0,
							"hasChildren" => false,
					));
				}
				array_push($arr,array(
						"id"=>"node-" . $k,
						"text"=>"{$value['title']}",
						"value" =>"{$value['id']}",
						"showcheck"=>false,
						complete => true,
						"isexpand" => false,
						"checkstate"=>0,
						"hasChildren" => true,
						"ChildNodes" => $subarr
				));
				$root['ChildNodes'] = $arr;
			}
			$select_categorys = json_encode(array($root));

			$multi_select = M('Keywords')->field('id,keyword,first_letter')->order('search_num DESC')->select();
 			$this->assign('tpltitle','添加');
			$this->assign('select_categorys',$select_categorys);
			$this->assign('multi_select',$multi_select);
			$this->display();
		}
	}

	/* 编辑店铺 */
	public function edit() {
		$id = $this->_get('id','intval',0);
		if(!$id)$this->error('参数错误!');
		$where['id'] = $id;
		$where['mall_id'] = session('mall_id');
		$edit = $this->store_model->where($where)->find();
		if (IS_POST) {
			$model_keywords = M('Keywords');  // 关键字数据表
			$_POST['mall_id']=session('mall_id');
			$_POST['mall_name']=session('mall_name');
			/* 店铺分类 start */
			$checked_categorys = array();
			$add_cate_keyword_ids = array();
			$select_cate_keyword_ids = array();
			$have_cate_keyword_ids = array();
			$add_cate_keywords = array();
			if (isset($_POST["checked_categorys"]) && !empty($_POST["checked_categorys"]) ) {
				$_POST['category_ids'] = $_POST["checked_categorys"]; // 分类id
				$checked_categorys = explode(',',$_POST["checked_categorys"]);
				$categorys_keyword_list= M('product_cat')->field('title')->where(array('id'=>array('IN',$checked_categorys)))->select();
				$add_cate_keywords = $this->getSubByKey($categorys_keyword_list, 'title');

				$have = $model_keywords->field('id,keyword')->where(array('keyword'=>array('IN',$add_cate_keywords)))->select();
				if($have){
					$have_cate_keyword_ids = $this->getSubByKey($have, 'id');
					$have_cate_keywords = $this->getSubByKey($have, 'keyword');
					$add_cate_keywords = array_diff($add_cate_keywords,$have_cate_keywords);// 去掉已有关键字
				}
				foreach($add_cate_keywords as $keyword) { // 将新关键字存入数据库并获取id
					$add_cate_keyword_ids[] = $model_keywords->add(array('keyword'=>$keyword));
				}
				$select_cate_keyword_ids = array_merge($have_cate_keyword_ids,$add_cate_keyword_ids);
				unset($_POST["checked_categorys"]);
			}
			/* 店铺关键字 start*/
			$select_keyword_ids = array(); //
			if (isset($_POST["select_keywords"]) && !empty($_POST["select_keywords"]) ) {
				$select_keyword_ids = explode(',',$_POST["select_keywords"]);
				unset($_POST["select_keywords"]);
			}
			/* 添加关键字入数据库 */
			$have_keyword_ids = array(); // 存放已有关键字id
			$have_keywords = array();    // 存放已有关键字
			$add_keyword_ids = array();  // 存放添加关键字id
			if (isset($_POST["add_keywords"]) && !empty($_POST["add_keywords"])) {
				$add_keywords = explode(',',$_POST["add_keywords"]);
				$have = $model_keywords->field('id,keyword')->where(array('keyword'=>array('IN',$add_keywords)))->select();
				if($have){
					$have_keyword_ids = $this->getSubByKey($have, 'id');
					$have_keywords = $this->getSubByKey($have, 'keyword');
					$add_keywords = array_diff($add_keywords,$have_keywords);
				}
				foreach($add_keywords as $keyword) {
					$add_keyword_ids[] = $model_keywords->add(array('keyword'=>$keyword));
				}
				$add_keyword_ids = array_merge($have_keyword_ids,$add_keyword_ids);
				unset($_POST["add_keywords"]);
			}
			// 整合所有keyword的id
			$select_keyword_ids = array_merge($select_cate_keyword_ids,$select_keyword_ids,$add_keyword_ids);
			/* 如果选择了关键字 */
			if($select_keyword_ids){
				$select_keyword_ids = array_unique($select_keyword_ids);
			}
			$_POST['fix_time']=time();
			$result = $this->store_model->where($where)->save($_POST);
			if($result){
				/*多对多 分类-店铺*/
				$Cat_store_model = M('Cat_store');
				$Cat_store_model->where(array('store_id'=>$id))->delete();
				foreach($checked_categorys as $v){
					$Cat_store_model->add(array('store_id'=>$id,'category_id'=>$v));
				}
				/*多对多 关键字-店铺*/
				$Keyword_store_model = M('Keyword_store');
				$Keyword_store_model->where(array('store_id'=>$id))->delete();// 先删除，再添加
				foreach($select_keyword_ids as $v) {
					$Keyword_store_model->add(array('store_id'=>$id,'keyword_id'=>$v));
				}
				$this->success('修改成功');
			} else {
				$this->error('修改失败');
			}
		} else {
			$Cat_store_list = M('Cat_store')->field('GROUP_CONCAT(category_id) category_id')->where(array('store_id'=>$id))->find();
			$category_ids = explode(',',$Cat_store_list["category_id"]);
			$Product_cat_pids = M('Product_cat')->field('DISTINCT GROUP_CONCAT(pid) pid')->where(array('id'=>array('IN',$category_ids)))->find();
			$category_pids = explode(',',$Product_cat_pids["pid"]);
			/*分类*/
			$root = array("id"=>"0",  /*根*/
					"text"=>"选择分类",
					"value" =>"0",
					"showcheck"=>false,
					complete => true,
					"isexpand" => $Cat_store_list["category_id"]?true:false,
					"checkstate"=>0,
					"hasChildren" => true);
			$Product_cat = D('Product_cat')->field('id,title')->where('pid=0')->select();
			$arr = array();
			foreach($Product_cat as $k=>$value){
				$subarr  = array();
				$Product_cat_son = D('Product_cat')->field('id,title')->where('pid='.$value['id'])->select();
				foreach($Product_cat_son as $kk => $vv){
					array_push($subarr,array(   /*二级分类*/
							"id"=>"node-" . $k.'-'.$kk,
							"text"=>"{$vv['title']}",
							"value" =>"{$vv['id']}",
							"showcheck"=>true,
							complete => true,
							"isexpand" => true,
							"checkstate"=>in_array($vv['id'], $category_ids)?1:0,
							"hasChildren" => false,
					));
				}
				 array_push($arr,array(  /*一级分类*/
					    "id"=>"node-" . $k,
						"text"=>"{$value['title']}",
						"value" =>"{$value['id']}",
						"showcheck"=>false,
						complete => true,
						"isexpand" => in_array($value['id'], $category_pids)?true:false,
						"checkstate"=>0,
						"hasChildren" => true,
					    "ChildNodes" => $subarr
				 ));
				$root['ChildNodes'] = $arr;
			}
			$select_categorys = json_encode(array($root));

			$selected_keyword_ids = M('Keyword_store')->field('GROUP_CONCAT(keyword_id) keyword_id')->where(array('store_id' => $id))->find();
			$selected_keyword_ids = $selected_keyword_ids['keyword_id'];
			if($selected_keyword_ids){
				$sql = "SELECT `id`,`keyword`,`first_letter`,instr(',{$selected_keyword_ids},',concat(',',id,',')) selected FROM `pigcms_keywords` ORDER BY selected DESC";
				$multi_select = M()->query($sql);
			} else {
				$multi_select = M('Keywords')->field("id,keyword,first_letter,instr(',6,11,26,27,28,29,30,',concat(',',id,','))")->order("search_num DESC")->select();
			}

			$this->assign('id',$id);
			$this->assign('edit',$edit);
			$this->assign('select_categorys',$select_categorys);
			$this->assign('multi_select',$multi_select);
			$this->display();
		}
	}
	/* 删除店铺 */
	public function del() {
		$id = $this->_get('id','intval',0);
		if(!$id)$this->error('参数错误!');
		$thisUser= $this->store_model->where(array('id'=>$id))->find();
		if($this->store_model->delete($id)){
			$this->assign("jumpUrl");
			$this->success('删除成功！');
		} else {
			$this->error('删除失败!');
		}
	}

}