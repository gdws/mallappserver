<?php
/**
 手机app地图管理
 **/
class HotgoodsAction extends BackAction{
	public $hotgoods_model;
	public function _initialize() {
		$this->hotgoods_model =M('Hotgoods');
		if(!session('mall_id')) {
			$where['admin_id'] = $_SESSION[C('USER_AUTH_KEY')];
			$mall = M('mall')->where($where)->find();
			if(!$mall) {
				exit('请先配置购物中心');
			}
			session('mall_id',$mall['id']);
		}
        parent::_initialize();  //RBAC 验证接口初始化
    }
    public function index() {
		$hotgoods_model = M('Hotgoods');
		$where = array('mall_id'=>session('mall_id'));
		if (IS_POST) {
			$map['token'] = $this->get('token');
//			$map['name|des'] = array('like', '%' . $key . '%');
			$list = $hotgoods_model->where($map)->order('sort ASC, id DESC')->select();
			$count = $hotgoods_model->where($map)->count();
			$Page = new Page($count, 20);
			$show = $Page->show();
		} else {
			$count = $hotgoods_model->where($where)->count();
			$Page = new Page($count, 5);
			$show = $Page->show();
			/* 查询同一级别的分类 */
			$list = $hotgoods_model->where($where)->order('sort ASC, id DESC')->limit($Page->firstRow . ',' . $Page->listRows)->select();
		}
		$this->assign('page', $show);
		$this->assign('list', $list);  // 分类列表
		$this->display();
	}
	/* 添加购物中心 */
	public function add()
	{
		if (IS_POST) {
			$_POST['mall_id']=session('mall_id');
			$_POST['starttime'] = strtotime($_POST['starttime']);
			$_POST['endtime'] = strtotime($_POST['endtime']);
			$mall_id = $this->hotgoods_model->add($_POST);
			if($mall_id){
				$this->success('添加成功');
			} else {
				$this->error('添加失败');
			}
		} else {
			$this->display();
		}
	}
	/* 编辑购物中心 */
	public function edit(){
		$id = $this->_get('id','intval',0);
		if(!$id)$this->error('参数错误!');
		$where['id'] = $id;
		$hotgoods  = $this->hotgoods_model->where($where)->find();
		if (IS_POST) {
			$_POST["starttime"]=strtotime($_POST["starttime"]);
			$_POST["endtime"]=strtotime($_POST["endtime"]);
			if($this->hotgoods_model->where($where)->save($_POST)){
				$this->success('修改成功');
			}else{
				$this->error('修改失败');
			}
		} else {
			$this->assign('set', $hotgoods);
			$this->display();
		}
	}
	/* 删除爆品 */
	public function del(){
		$id = $this->_get('id','intval',0);
		if(!$id)$this->error('参数错误!');
		$thisUser= $this->hotgoods_model->where(array('id'=>$id))->find();
		if($this->hotgoods_model->delete($id)){
			$this->assign("jumpUrl");
			$this->success('删除成功！');
		} else {
			$this->error('删除失败!');
		}
	}
}