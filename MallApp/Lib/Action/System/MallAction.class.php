<?php
/**
 手机app地图管理
 **/
class MallAction extends BackAction{
	public function _initialize() {
        parent::_initialize();  //RBAC 验证接口初始化
    }
	public function index() {
		$mall_model=M('Mall');
		$pid=$this->_get('pid','intval');  // 节点id(node表的id)
		$where['admin_id'] = $_SESSION[C('USER_AUTH_KEY')];
		$mall = $mall_model->where($where)->find();
		/* 分类 */
		$parentid = (isset($_GET['parentid']) ? intval($_GET['parentid']) : 0);// 分类的父亲id
		$data = M('Product_cat');
		$where = array('mall_id' => $mall['id'],'parentid' => $parentid);
			if ($mall) {
				session('mall_id',$mall['id']);
				$this->redirect('Mall/edit');
			}
			else {
				$this->error('您还没有添加您的购物中心信息', U('Mall/add'));
			}
		$this->assign('pid',$pid);
		$this->assign('$mall',$mall);
		$this->display();
	}
	/* 添加购物中心 */
	public function add()
	{
		$mall_model = M('Mall');
		$where['admin_id'] = $_SESSION[C('USER_AUTH_KEY')];
		$mall = $mall_model->where($where)->find();
		if($mall){
			$this->error('已经有购物中心',U('Mall/edit'));
		}
		if (IS_POST) {
			unset($_POST["__hash__"]);
			$_POST['admin_id']=$_SESSION[C('USER_AUTH_KEY')];
			$_POST['add_time'] = time();
			$mall_id = $mall_model->where($where)->add($_POST);
			if($mall_id){
				$this->success('添加成功',U('Mall/index'));
			} else {
				$this->error('添加失败');
			}
		} else {
			$this->display();
		}
	}
	/* 编辑购物中心 */
	public function edit() {
		$mall_model = M('Mall');
		$where['admin_id'] = $_SESSION[C('USER_AUTH_KEY')];
		$mall = $mall_model->where($where)->find();
		if (IS_POST) {
			if($mall_model->where($where)->save($_POST)){
				$this->success('修改成功');
			} else {
				$this->error('修改失败');
			}
		} else {
			$this->assign('set', $mall);
			$this->display();
		}
	}


}