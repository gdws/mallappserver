<?php
/**
 手机app地图管理
 **/
class MapAction extends BackAction{
	public $token;
	public $apikey;
	public function _initialize() {
		parent::_initialize();
		$this->token=session('token');
		$this->assign('token',$this->token);
		$this->apikey=C('baidu_map_api');
		$this->assign('apikey',$this->apikey);
	}
	public function index() {
		 echo '地图编辑';
	}

	public function setLatLng_amap() {
		$amap=new amap();
		$this->assign('key',$amap->key);
		$this->display();
	}
}
