<?php
/*
 * 店铺管理
 */
class KeywordsAction extends BackAction
{
	public $Model;
	public function _initialize() {
		$this->Model = M('Keywords');
		// 判断是否存在购物中心
		if(!session('mall_id')) {
			$where['admin_id'] = $_SESSION[C('USER_AUTH_KEY')];
			$mall = M('mall')->where($where)->find();
			if (!$mall) {
				$this->error('您还没有添加您的购物中心信息', U('Mall/add'));
			}
			session('mall_id',$mall['id']);
		}
		parent::_initialize();  //RBAC 验证接口初始化
	}
	// 关键字列表
	public function index() {
		if (IS_POST) { // 搜索
			$map['token'] = $this->get('token');
//			$map['name|des'] = array('like', '%' . $key . '%');
			$list = $this->Model->where($map)->order('sort ASC, id DESC')->select();
			$count = $this->Model->where($map)->count();
			$Page = new Page($count, 20);
			$show = $Page->show();
		} else {
			$count = $this->Model->count();
			$Page = new Page($count, 15);
			$show = $Page->show();
			$list = $this->Model->order('search_num DESC,first_letter ASC')->limit($Page->firstRow . ',' . $Page->listRows)->select();
		}
	//		dump($list);
	//		die;
		foreach($list as $k => &$vo){
//			$store_ids = explode(',',$vo['store_ids']);
			if(!empty($vo['store_ids'])){
				$map['id']  = array('in',$vo['store_ids']);
				$Store_list = M('Store')->field('GROUP_CONCAT(name) store_name')->where($map)->find();
				$vo['store_name'] = $Store_list['store_name'];
			}else{
				$vo['store_name'] = '';
			}
		}
		$this->assign('page', $show);
		$this->assign('list', $list);
		$this->display();
	}
	/* 添加关键字 */
	public function add()
	{
		if (IS_POST) {
			$_POST['add_time']=time();
			$category = explode('_',$_POST['category']);
			$_POST['category_id']=$category[0];
			$_POST['category_name']=$category[1];
			if (isset($_POST["checkvalue"]) && !empty($_POST["checkvalue"]) ) {
				$_POST['category_ids'] = $_POST["checkvalue"]; // 分类id
				$checkvalue = explode(',',$_POST["checkvalue"]);
				unset($_POST["checkvalue"]);
			}
			$store_id = $this->Model->add($_POST);
			if($store_id){
				foreach($checkvalue as $v){
					M('Cat_store')->add(array('store_id'=>$store_id,'category_id'=>$v));
				}
				$this->success('添加成功');
			} else {
				$this->error('添加失败');
			}
		} else {
			// 节点树
			$root = array("id"=>"0",
					"text"=>"选择分类",
					"value" =>"0",
					"showcheck"=>false,
					complete => true,
					"isexpand" => false,
					"checkstate"=>0,
					"hasChildren" => true);
			$Product_cat = D('Product_cat')->field('id,title')->where('pid=0')->select();
			$arr = array();
			foreach($Product_cat as $k=>$value){
				$subarr  = array();
				$Product_cat_son = D('Product_cat')->field('id,title')->where('pid='.$value['id'])->select();
				foreach($Product_cat_son as $kk => $vv){
					array_push($subarr,array(
							"id"=>"node-" . $k.'-'.$kk,
							"text"=>"{$vv['title']}",
							"value" =>"{$vv['id']}",
							"showcheck"=>true,
							complete => true,
							"isexpand" => true,
							"checkstate"=>0,
							"hasChildren" => false,
					));
				}
				array_push($arr,array(
						"id"=>"node-" . $k,
						"text"=>"{$value['title']}",
						"value" =>"{$value['id']}",
						"showcheck"=>false,
						complete => true,
						"isexpand" => false,
						"checkstate"=>0,
						"hasChildren" => true,
						"ChildNodes" => $subarr
				));
				$root['ChildNodes'] = $arr;
			}
			$select_categorys = json_encode(array($root));

			$this->assign('tpltitle','添加');
			$this->assign('select_categorys',$select_categorys);
			$this->display();
		}
	}
	/* 编辑关键字 */
	public function edit() {
		$id = $this->_get('id','intval',0);
		if(!$id)$this->error('参数错误!');
		$where['id'] = $id;
//		$where['mall_id'] = session('mall_id');
		$edit = $this->Model->where($where)->find();
		if (IS_POST) {
			if (isset($_POST["selectvalue"]) && !empty($_POST["selectvalue"]) ) {
				$_POST["store_ids"] = $_POST["selectvalue"];
				$Keyword_store_model = M('Keyword_store');
				$Keyword_store_model->where(array('keyword_id'=>$id))->delete();
				$selectvalue = explode(',',$_POST["selectvalue"]);
				foreach($selectvalue as $v) {
					$Keyword_store_model->add(array('keyword_id'=>$id,'store_id'=>$v));
				}
				unset($_POST["selectvalue"]);
			}
			$result = $this->Model->where($where)->save($_POST);
			if($result){
				$this->success('修改成功');
			} else {
				$this->error('修改失败');
			}
		} else {
			$select_categorys = M('Store')->field('id,name')->order('id DESC')->select();
			$store_ids_arr =explode(',',$edit["store_ids"]);
			foreach($select_categorys as &$vo){
				 $vo['selected'] = in_array($vo['id'],$store_ids_arr)?'selected':'';
			}
			$this->assign('id',$id);
			$this->assign('edit',$edit);

			$this->assign('select_categorys',$select_categorys);
			$this->display();
		}
	}
	/* 删除关键字 */
	public function del() {
		$id = $this->_get('id','intval',0);
		if(!$id)$this->error('参数错误!');
		$thisUser= $this->Model->where(array('id'=>$id))->find();
		if($this->Model->delete($id)){
			$this->assign("jumpUrl");
			$this->success('删除成功！');
		} else {
			$this->error('删除失败!');
		}
	}
}