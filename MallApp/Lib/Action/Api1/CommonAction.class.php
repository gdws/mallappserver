<?php
class CommonAction extends Action {
    /*
     * 升级接口
     */
    public function Init(){

    }
    // 获取验证码 (异步处理) 不是接口
    function async_getCaptcha($type=1,$mobile='') {
        $model_verification_code =  M('verification_code');
        $url = "http://121.199.16.178/webservice/sms.php?method=Submit";
        $account = 'cf_bannian';  # 用户名
        $password = 'bannian123'; # 密码
        $randomCode = rand(1000, 9999); # 我们的设定的6位验证码
        $today = date('Y-m-d');
        $curlPost = "account=" . $account . "&password=" . $password . "&mobile=" . $mobile . "&content=" . rawurlencode("您的验证码是：" . $randomCode . "。请不要把验证码泄露给其他人。"); // rawurlencode 将字符串编码成 URL 专用格式。
        $gets = curl_post($curlPost, $url);     # 消息内容 #跳转url
        $xml = simplexml_load_string($gets);    # 解析XML
        $code = $xml->code;                     # 获取对象为code的
        $data = array();
        $data['to_tel'] = $mobile;     # 电话号码
        $data['to_user'] = $mobile;    # 用户
        $data['code'] = $randomCode;   # 6位验证码
        $data['send_time'] = time();   # 获取时间
        $data['type'] = $type;         # 1:注册 2：找回密码  # 3:修改手机
        if ((int) $code == 4081) {
            $data['status'] = 2;     //  同一手机号码一分钟之内发送频率不能超过1条
            $model_verification_code->add($data);
        } else if ((int) $code == 4082 || (int) $code == 4085) {
            $data['status'] = 3;    //  短信发送超过上限，请联系客服
            $model_verification_code->add($data);
        } else if ((int) $code == 2) {  # 2 表示发送成功
            $data['status'] = 1;
            $model_verification_code->add($data);
        } else {
            $data['status'] = 0;  // 失败
            $model_verification_code->add($data);
        }
    }

    /*
    *  响应数据
    * @errcode  错误码
    * @errmsg   错误信息
    * @data     数据
    */
    public function Response($errcode=0,$errmsg='',$data=array()) {
        $response = array('errcode' => $errcode, 'errmsg' => $errmsg,'data' => $data);
        $this->ajaxReturn($response);
    }

    /*string类型转换int或float*/  // 用&赋值
    function arrStr2int(&$arr=array(),$fields=array()){
        if(!is_array($arr)) return $arr;
        if(!is_array($fields)) return false;
        foreach($arr as $k=>&$v){
            foreach($fields as $vv){
                $v[$vv]+=0;
            }
        }
    }
}