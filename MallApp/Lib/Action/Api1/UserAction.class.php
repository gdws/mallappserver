<?php
/* 版本 1.0
 * http://localhost/index.php?m=Api10&c=User&a=方法名
 * http://weixin.goldenwingsinfo.com/mallapp1/index.php?m=Api10&c=User&a=ConfirmCaptcha
 *本地访问：
 * http://192.168.2.222/index.php?m=Api10&c=User&a=Login
 *
 * http://192.168.1.103/index.php?m=Api10&c=User&a=Search
 *
 */
class UserAction extends CommonAction {
    /*
     * 获取验证码   GetPhoneCode
     * @username  手机号
     * @type      类型 1:注册获取
     */
    public function GetPhoneCode() {
            $mobile = trim($_REQUEST['username']);   # 注册人手机号
            $type = intval($_REQUEST['type']);       # 类型
        switch ($type) {
            case 1:
                $is_mobile_exists = M('users')->where("username='{$mobile}'")->find();  # 查看此帐号是否存在
//                if($is_mobile_exists){
//                    parent::Response(1,'已经存在',array());
//                }
                break;
            default:
                '';
                break;
        }
            // 限制
            $verification_code = M('verification_code')->field('status,send_time')->where("to_tel='{$mobile}'")->order('send_time DESC')->find();   // 服务器记录的验证码
            $sms_status = $verification_code['status'];
            $send_time = $verification_code['send_time'];
            if(($send_time+60) > time()){
                parent::Response(1,'同一手机号码一分钟之内发送频率不能超过1条',array());
            }
            $today = date('Y-m-d');
            if ($today == date('Y-m-d',$send_time)) {  // 是否是今天
                if($sms_status == 3){ // 短信发送超过上限
                    parent::Response(2,'短信发送超过上限，请联系客服',array());
                }
            }
            /* 执行获取验证码 */
            parent::async_getCaptcha($type,$mobile);
            parent::Response(0,'success',array());
    }

    /* 注册和登录一个接口
    * 会员登录  Login
    * @username 手机号
    * @code     验证码
    */
    function Login() {
        $username = trim($_REQUEST['username']);    # 用户名  18628018903
        $captcha = trim($_REQUEST['code']);      # 客户端获取的验证码
        $model_verification_code = M('verification_code');
        $ServerCode = $model_verification_code->where("to_tel='{$username}'")->order('send_time DESC')->getField('code');   // 服务器记录的验证码
            if ($captcha <> $ServerCode)
                parent::Response(1,'此手机验证码错误',array());  // 验证码错误
            $sms_log = $model_verification_code->where("code='{$captcha}' AND send_time between UNIX_TIMESTAMP()-1800  AND UNIX_TIMESTAMP() AND to_tel='{$username}'")->order('send_time DESC')->find();
            if (!$sms_log)
                parent::Response(2,'此验证码已超时',array()); # status  超时
            /* 成功后：用户名存入数据库 */
            $model_Users = D('Users');
            $model_Users->addUser($username);
            parent::Response(0,'',array()); # 成功
    }
    /*
    *   搜索 Search
    *   keyword 搜索关键字
    */
    public function Search(){
        $keyword = $_REQUEST['keyword'];
        $keyword = '店铺';
        $where = array();
        $where['name'] = array('like',"%{$keyword}%");
        $ret =  M('store')->field("id as store_id,name,sc_id,sc_name")->where($where)->select();
        parent::Response(0,'',$ret); # 成功
    }
    /*
     * 获取用户信息
     */
    public function UserInfo(){
        echo THINK_VERSION;
    }
}