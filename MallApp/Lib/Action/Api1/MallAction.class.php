<?php
/* 版本 1.0
 */
class MallAction extends CommonAction {
    public function test() {

    }
        /* http://www.mallapp.com/index.php?g=Api1&m=Mall&a=Malls
            &sequence=0&city=成都
         * 购物中心列表   Malls
         * @sequence   本接口版本号 0,
         * @city       城市
         *
         * 返回状态码：errcode 0:成功返回 1：sequence相同不更新
         */
    public function Malls() {
        file_put_contents('./test/Malls.log',serialize($_REQUEST));
        $clientSequence = $_REQUEST['sequence'];    // 客户端
        $svrSequence = M('sequence')->getField('sequence', "`name`='Malls'");  // 服务端序列
        if ($clientSequence == $svrSequence){     //  不刷新
            $ret = array('sequence'=>intval($svrSequence),'list'=>array());
            parent::Response(1,'',$ret);
        }
        # 热门推荐 start
        $city = urldecode($_REQUEST['city']);
        $where['city'] = array('LIKE',"%{$_REQUEST['city']}%");
        $malls = M('mall')->field('id,name,latitude,longitude,logourl,address')->where($where)->select();
        if(!$malls){
            parent::Response(0,'',array());
        }
        parent::arrStr2int($malls,array('id','latitude','longitude'));
        $ret = array('sequence'=>intval($svrSequence),'list'=>$malls);
        parent::Response(0,'',$ret);
    }

    /* http://www.mallapp.com/index.php?g=Api1&m=Mall&a=HotSearch&sequence=0
     * 热门搜索   HotSearch
     * @sequence   本接口版本号
     *
     * 返回状态码：errcode 0:成功返回 1：sequence相同不更新
     */
    public function HotSearch() {
        $clientSequence = $_REQUEST['sequence'];    // 客户端
        $svrSequence = M('sequence')->getField('sequence', "`name`='HotSearch'");  // 服务端序列
        if ($clientSequence == $svrSequence){     //  不刷新
            $ret = array('sequence'=>intval($svrSequence),'list'=>array());
            parent::Response(1,'',$ret);
        }
//        $where['city'] = array('LIKE',"%%");
        $malls = M('Keywords')->field('id,keyword')->where('1=1')->order('search_num DESC')->limit(20)->select();
        if(!$malls){
            parent::Response(0,'',array());
        }
        parent::arrStr2int($malls,array('id'));
        $ret = array('sequence'=>intval($svrSequence),'list'=>$malls);
        parent::Response(0,'',$ret);
    }
    /*
     * 店铺搜索   StoreSearch
     * @keyword  关键字
     * @uid      用户id
     * @mall_id  购物中心id
     * @city     城市
     *
      http://www.mallapp.com/index.php?g=Api1&m=Mall&a=StoreSearch
      &keyword=餐饮&uid=9948&mall_id=6&city=成都
     */
    public function StoreSearch() {
        $keyword = urldecode($_REQUEST['keyword']);
        $uid = $_REQUEST['uid'];
        $mall_id = intval($_REQUEST['mall_id']);
        $city = urldecode($_REQUEST['city']);
        /*处理关键字*/
        $Model_Keywords = M('Keywords');
        $checkdata = $Model_Keywords->where(array('keyword'=>$keyword))->find();
        $data = array();
        if($checkdata){
            $Model_Keywords->where("keyword='{$keyword}'")->setInc('search_num');
        }else{
            $Pin = new Pinyin();
            $pinyin = $Pin->Pinyin($keyword);
            $firstLetter = strtoupper(substr($Pin->Pinyin($keyword),0,1));
            $data['keyword'] = $keyword;
            $data['search_num'] = 1;
            $data['pinyin'] = $pinyin;
            $data['first_letter'] = $firstLetter;
            $Model_Keywords->add($data);
        }
        /*记录搜素日志*/
            $data = array();
            $Model_search_log = M('search_log');
            $data['uid'] = $uid;
            $data['keyword'] = $keyword;
            $data['city'] = $city;
            $data['search_time'] = time();
            $data['mall_id'] = $mall_id;
            $Model_search_log->add($data);
        /*查询出结果*/
        $Model = D("StoreView");
        // 如果存在购物中心id就查询此购物中心内的店铺，否则查询所有购物中心的店铺
        if($mall_id){
            $where['store.mall_id'] = $mall_id;
        } else {

        }
        $where['keywords.keyword'] = array('like', "%{$keyword}%");
//        $list = $Model->where("keywords.keyword LIKE '%{$keyword}%'")->order('id desc')->limit(20)->select();
        $list = $Model->where($where)->order('id desc')->limit(20)->select();
        if(!$list){
            parent::Response(0,'没有数据',array());
        }

        parent::arrStr2int($list,array('id','mall_id','average_price','is_groupon','is_coupon','is_new'));
        parent::Response(0,'',$list);
    }
    /*
     * 爆品列表   Hotgoods
     * @username  手机号
     */
    public function Hotgoods() {
       $ret =  array('groupon'=>0,'kkk'=>1);
       echo json_encode($ret);
    }
    /*
     * 分类       Category
     * @username  手机号
     */
    public function Category() {

    }
    /*
     * 店铺列表       Store
     * @username  手机号
     */
    public function Store() {

    }
    /*
     * 收藏
     */
    public function Collect(){

    }
}