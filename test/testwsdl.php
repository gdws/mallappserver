<?php
//http://www.gdws-pigcms.com/test/testwsdl.php
ini_set('soap.wsdl_cache_enabled','0');//关闭缓存
header ( "Content-Type: text/html; charset=utf-8" );
/*
* 指定WebService路径并初始化一个WebService客户端
*/
//$ws = "http://192.168.1.241:2088/CRM_VIP_Proxy.asmx?WSDL";// webservice服务的地址
$ws = "http://192.168.1.241:2008/TTPOS/vip.asmx?WSDL";// webservice服务的地址
try {
    $client = new SoapClient($ws);//这个SOAP地址要换成你自己的
//获取接口所有方法及参数
    $requestXML = '<?xml version="1.0" encoding="utf-8"?><xml>
   <astr_request>
        <header>
          <licensekey>string</licensekey>
          <username>string</username>
          <password>string</password>
          <lang>string</lang>
          <pagerecords>int</pagerecords>
          <pageno>int</pageno>
          <updatecount>int</updatecount>
          <messagetype>string</messagetype>
          <messageid>string</messageid>
          <version>string</version>
        </header>
        <search>
          <vipcode></vipcode>
          <name></name>
          <telephone></telephone>
          <vipid>51092119770630865X</vipid>
          <vipemail></vipemail>
          <address1></address1>
          <sex></sex>
          <issuedate_yyyymmdd>0</issuedate_yyyymmdd>
          <issuestore>0</issuestore>
          <fromage>0</fromage>
          <toage>0</toage>
          <frombirthdaymm>0</frombirthdaymm>
          <tobirthdaymm>0</tobirthdaymm>
          <frombirthdaydd>0</frombirthdaydd>
          <tobirthdaydd>0</tobirthdaydd>
          <fromcurrentbonus>0</fromcurrentbonus>
          <tocurrentbonus>0</tocurrentbonus>
          <fromaccumulatedsalesamt>0</fromaccumulatedsalesamt>
          <toaccumulatedsalesamt>0</toaccumulatedsalesamt>
          <fromaccumulatedbonus>0</fromaccumulatedbonus>
          <toaccumulatedbonus>0</toaccumulatedbonus>
          <vipgroup>
            <vipgrouprange xsi:nil="true" />
            <vipgrouprange xsi:nil="true" />
          </vipgroup>
          <activitycount>0</activitycount>
          <fromsalestxdate_yyyymmdd>0</fromsalestxdate_yyyymmdd>
          <tosalestxdate_yyyymmdd>0</tosalestxdate_yyyymmdd>
          <itemgroup>
            <itemgrouprange xsi:nil="true" />
            <itemgrouprange xsi:nil="true" />
          </itemgroup>
          <salesamount>0</salesamount>
          <loginname>0</loginname>
          <receivenewsletter>0</receivenewsletter>
          <preferencelang>0</preferencelang>
          <excond1>0</excond1>
          <excond2>0</excond2>
          <excond3>0</excond3>
          <excond4>0</excond4>
          <excond5>0</excond5>
        </search>
        <calledbyint>0</calledbyint>
      </astr_request>
      </xml>';

    function xml_to_array( $xml )
    {
        $reg = "/<(\\w+)[^>]*?>([\\x00-\\xFF]*?)<\\/\\1>/";
        if(preg_match_all($reg, $xml, $matches))
        {
            $count = count($matches[0]);
            $arr = array();
            for($i = 0; $i < $count; $i++)
            {
                $key= $matches[1][$i];
                $val = xml_to_array( $matches[2][$i] );  // 递归
                if(array_key_exists($key, $arr))
                {
                    if(is_array($arr[$key]))
                    {
                        if(!array_key_exists(0,$arr[$key]))
                        {
                            $arr[$key] = array($arr[$key]);
                        }
                    }else{
                        $arr[$key] = array($arr[$key]);
                    }
                    $arr[$key][] = $val;
                }else{
                    $arr[$key] = $val;
                }
            }
            return $arr;
        }else{
            return $xml;
        }
    }
// 执行方法
    function xmltoarray( $xml ) // Xml 转 数组, 不包括根键
    {
        $arr = xml_to_array($xml);
        $key = array_keys($arr);
        return $arr[$key[0]];
    }
    $arr = xmltoarray($requestXML);
    print_r($arr);die;


    function xml_to_arr($xml_obj) {
        foreach ($xml_obj as $key => $value) {
            $data[$key] = strval($value);
            if(count($value)>0){
                $data[$key] =  xml_to_arr($value); // 递归调用
            }
        }
        return $data;
    }
    /* 测试 */
    $xml_obj = new SimpleXMLElement($requestXML);
    $param = xml_to_arr($xml_obj);
    echo "<pre>";
    print_r($param);die;

    $function = 'getvipgroupmaster';
    $result = $client->__soapCall($function, array('parameters' => $param));
    if (is_soap_fault($result))
    {
        trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
    }
    else
    {
//        $data = $result->$function.'Result'; //这里返回的是类，必须使用->得到元素的值
        $obj = $function.'Result';
        $data =  $result->$obj;
        echo "<pre>";
        print_r($data);
    }
}
catch (SoapFault $e)
{
    printf("Exception".$e);
}